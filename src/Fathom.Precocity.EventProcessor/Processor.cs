using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Concurrent;
using Microsoft.Extensions.Logging;
using Fathom.Precocity.EventProcessor.Component;

namespace Fathom.Precocity.EventProcessor{
    public class Processor<R>{
        private ItemReader<R> itemReader;
        private ItemProcessor<R> itemWriter;

        private IErrorHandler<R> errorHandler;

        private ILogger logger;

        private int consumerInstances = 1;

        private int threadpoolSize = 20;
        private int numberOfRetries = 3;

        private int batchSize = 1;

        private Object shutDownLock = new Object();

        private Boolean isShutDownCalled = false;

        private CountdownEvent countDownLatch;//can be replaced with semaphore as well

        private SemaphoreSlim semaphoreForProcessorThreadPool;//concurrent threads to be used in threadpool(throttling)

        private CancellationTokenSource cts = new CancellationTokenSource();

        private HashSet<ItemReader<R>> itemReaders = new HashSet<ItemReader<R>>();

        public void SetRetries(int numberOfRetries){
            if(numberOfRetries <= 0){
                numberOfRetries = 1;
            }
            this.numberOfRetries = numberOfRetries;
        }

        public void SetBatchSize(int batchSize){
            this.batchSize = batchSize;
        }

        public void SetConsumerInstances(int consumerInstances){
            this.consumerInstances = consumerInstances;
        }

        public void SetThreadpoolSize(int threadpoolSize){
            this.threadpoolSize = threadpoolSize;
        }

        public Processor(ItemReader<R> itemReader, ItemProcessor<R> itemWriter, IErrorHandler<R> errorHandler, ILogger logger){
            this.itemReader = itemReader;
            this.itemWriter = itemWriter;
            this.errorHandler = errorHandler;
            this.logger = logger;
        }

        public async Task<int> Start(){
            try{
                logger.LogInformation("Starting processor");
                countDownLatch = new CountdownEvent(this.consumerInstances);
                semaphoreForProcessorThreadPool = new SemaphoreSlim(threadpoolSize);
                for(int i=0; i< consumerInstances; i++){
                    ItemReader<R> reader = itemReader.DeepClone();
                    itemReaders.Add(reader);
                    Thread thread = new Thread(() => StartConsumer(reader, cts));
                    thread.Start();
                }
                return await HandleProcessExit();
            }
            finally{
                this.Stop();
            }
        }

        private void StartConsumer(ItemReader<R> itemReader, CancellationTokenSource cts){
            try{
            logger.LogInformation("Starting consumer");
            string uuid = Guid.NewGuid().ToString();

            while(!cts.Token.IsCancellationRequested){
                ItemRecord<R> itemRecord = null;
                R record = default(R);
                try{
                    record = itemReader.Consume(cts.Token);
                    itemRecord = new ItemRecord<R>(record);
                    logger.LogInformation("Consumed message: {record}, requestid: {requestid}, threadId: {threadid}"
                                , itemRecord.record, itemRecord.requestId
                                , Thread.CurrentThread.ManagedThreadId.ToString());
                    //TODO set batch here when required(Can use Rx) but can lead to event losses in case of force kill/server down etc
                    //If threads are available in threadpool, push to threadpool, else execute in same thread
                    //Similar to CallerRunsPolicy in java threadpool RejectedExecutionHandler
                    logger.LogInformation("Passing message to processor: {record}, requestid: {requestid}, threadId: {threadid}"
                                , itemRecord.record, itemRecord.requestId
                                , Thread.CurrentThread.ManagedThreadId.ToString());
                    if(this.semaphoreForProcessorThreadPool.Wait(0)){
                        Task.Run(() => this.ProcessEvent(new List<ItemRecord<R>>(){itemRecord}, true));
                    }
                    else{
                        this.ProcessEvent(new List<ItemRecord<R>>(){itemRecord}, false);
                    }
                    //TODO END
                }
                catch (Exception exception) {
                    logger.LogError("Error while Consuming : {record}, requestid: {requestid}, threadId: {threadid}"
                                , exception
                                , itemRecord.record, itemRecord.requestId
                                , Thread.CurrentThread.ManagedThreadId.ToString());
                    errorHandler.Handle(exception, new List<ItemRecord<R>>(){itemRecord});
                }
            }
            logger.LogInformation("Outside while loop");
            }
            finally{
                countDownLatch.Signal();
            }
        }

        private void ProcessEvent(List<ItemRecord<R>> records, bool isSubmittedToTask){
            int retry = this.numberOfRetries;
            try{
                while(retry > 0){
                    try{
                        itemWriter.Write(records);
                        break;
                    }
                    catch(Exception e){
                        if(retry <= 1){
                            errorHandler.Handle(e, records);
                        }
                    }
                    retry--;
                    Thread.Sleep(100);
                }
            }
            finally{
                if(isSubmittedToTask){
                    //Take care not to call this out of producer/consumer coupling!
                    this.semaphoreForProcessorThreadPool.Release();
                }
                records.Clear();
                records = null;
            }
        }
        public void Stop() {
            //Can be called from multiple places..Hence make sure only called once.
            logger.LogInformation("Shutdown called");
            lock(shutDownLock){
                if(isShutDownCalled){
                    logger.LogInformation("Shutdown already called..Skipping");
                    return;
                }
                this.isShutDownCalled = true;
                this.cts.Cancel();//End the while loop of consumers
                logger.LogInformation("Waiting for consumer threads to stop...");
                countDownLatch.Wait();//Wait for consumers to get out of while loop
                logger.LogInformation("Waiting for Processor threads to stop...");
                this.WaitForThreadpool();//Processor threads if submitted to threadpool
                logger.LogInformation("Disposing all resources...");
                this.DisposeAllResources();
                logger.LogInformation("Shutdown complete!");
            }
        }

        async Task<int> HandleProcessExit(){
            AppDomain.CurrentDomain.ProcessExit += (s, e) =>
            {
                //Console.WriteLine("ProcessExit!");
                this.Stop();
            };
            logger.LogInformation("Waiting for SIGTERM...");
            await Task.Delay(-1, cts.Token);
            return 0;
        }

        protected void WaitForThreadpool() {//to be called when threadpools/tasks used
            int retryCounter = 10;
            while(this.semaphoreForProcessorThreadPool.CurrentCount < this.threadpoolSize){
                logger.LogInformation("semaphore currentcount: " + semaphoreForProcessorThreadPool.CurrentCount);
                if(retryCounter <= 0){
                    logger.LogInformation("Force stopping!");
                    break;
                }
                logger.LogInformation("Waiting for tasks in threadpool to complete");
                Thread.Sleep(1000);
                retryCounter--;
            }
        }

        private void DisposeAllResources(){
            foreach(ItemReader<R> reader in itemReaders){
                if(reader != null){
                    reader.Close();
                }
            }

            itemReaders.Clear();

            if(itemWriter != null) {
                itemWriter.Close();
            }
            cts.Dispose();
            countDownLatch.Dispose();
            semaphoreForProcessorThreadPool.Dispose();
        }
    }
}