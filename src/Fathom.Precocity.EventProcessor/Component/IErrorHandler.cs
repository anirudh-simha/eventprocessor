using System;
using System.Collections.Generic;
namespace Fathom.Precocity.EventProcessor.Component
{
    public interface IErrorHandler<R>
    {
         void Handle(Exception exception, List<ItemRecord<R>> records);
    }
}