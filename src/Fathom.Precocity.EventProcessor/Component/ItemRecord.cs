using System;
namespace Fathom.Precocity.EventProcessor.Component{
    public class ItemRecord <R>{
        public R record{get; private set;}
        public string requestId{get; private set;}

        public ItemRecord(R record){
            this.record = record;
            this.requestId = Guid.NewGuid().ToString();
        }
    }
}