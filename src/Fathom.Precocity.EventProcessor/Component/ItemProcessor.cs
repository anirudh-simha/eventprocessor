namespace Fathom.Precocity.EventProcessor.Component
{
    using System.Collections.Generic;
    public interface ItemProcessor<R>
    {
         void Write(List<ItemRecord<R>> records);

         void Close();
         //writers are usually threadsafe hence deep clone not required here for now
    }
}