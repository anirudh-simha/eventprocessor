using System;
using System.Threading;
namespace Fathom.Precocity.EventProcessor.Component
{
    using System.Collections.Generic;
    public interface ItemReader<R>
    {
         R Consume(CancellationToken cts);
         ItemReader<R> DeepClone();

         void Close();


    }
}