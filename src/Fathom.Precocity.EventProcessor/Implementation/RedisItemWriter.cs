using System.Collections.Generic;
namespace Fathom.Precocity.FrameManager
{
    using StackExchange.Redis;
    using Fathom.Precocity.EventProcessor.Component;
    public abstract class RedisItemWriter<T> : ItemProcessor<T>
    {
        private string connectionString;
        private ConnectionMultiplexer redis;
        private IDatabase redisDatabase;

        protected RedisItemWriter(string connectionString){
            this.connectionString = connectionString;
            this.redis = ConnectionMultiplexer.Connect(connectionString);
            this.redisDatabase = redis.GetDatabase();
        }

        public abstract void Write(List<ItemRecord<T>> records);

        public void Close(){
            redis.Close();
            redis.Dispose();
        }
    }
}