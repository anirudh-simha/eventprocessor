using System;
using Serilog.Core;
using System.Collections.Generic;
namespace Fathom.Precocity.EventProcessor.Implementation
{
    using Fathom.Precocity.EventProcessor.Component;
    public class LoggerErrorHandler<R> : IErrorHandler<R>
    {//added serilog as logger here
        private Logger logger;
        public LoggerErrorHandler(Logger logger){
            this.logger = logger;
        }
        public void Handle(Exception exception, List<ItemRecord<R>> records){
            foreach(ItemRecord<R> record in records){
                logger.Error("Record {record} failed! Exception: {exception}, RequestId: {requestId}", record.record, exception.StackTrace, record.requestId);
            }
        }
    }
}