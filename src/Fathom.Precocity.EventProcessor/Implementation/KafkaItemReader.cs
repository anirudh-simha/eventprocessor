using System.Linq;
using System.Threading;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System;
namespace Fathom.Precocity.EventProcessor.Implementation{
    using Fathom.Precocity.EventProcessor.Component;
    using System.Collections.Generic;
    using Confluent.Kafka;
    using Newtonsoft.Json;

    public class KafkaItemReader<K,V> : ItemReader<V>
    {
        //TODO BUILDER PATTERN
        private List<string> topicList{get; set;}
        private List<int> partitionIds {get; set;}

        private int manualCommitInterval{get; set;}

        private ConsumerConfig config {get; set;}

        private IConsumer<K,V> consumer;
        private ConsumeResult<K, V> latestRecord;

        public void SetEnableAutoCommit(bool enableAutoCommit){
            config.EnableAutoCommit = enableAutoCommit;
        }

        public void SetAutoOffsetReset(AutoOffsetReset reset){
            config.AutoOffsetReset = reset;
        }

        public void SetAutoCommitInterval(int autoCommitIntervalMs){
            config.AutoCommitIntervalMs = autoCommitIntervalMs;
        }

        public KafkaItemReader(string brokerList, List<string> topicList, string groupId)
        {
            this.config = new ConsumerConfig();
            config.BootstrapServers = brokerList;
            config.GroupId = groupId;
            this.topicList = topicList;
            this.manualCommitInterval = 100;
            //{
            //    BootstrapServers = brokerList,
            //    GroupId = groupId,
            //    EnableAutoCommit = false,
                //StatisticsIntervalMs = 5000,
            //    SessionTimeoutMs = 6000,
            //    AutoOffsetReset = AutoOffsetReset.Earliest,
                //EnablePartitionEof = true
            //};
        }

        public KafkaItemReader(List<string> topicList, ConsumerConfig config, int manualCommitInterval, List<int> partitionList){
            this.topicList = topicList;
            this.config = config;
            this.manualCommitInterval = manualCommitInterval;
            this.partitionIds = partitionList;
            this.Setup();
        }

        public KafkaItemReader<K,V> Setup() {
            this.consumer = new ConsumerBuilder<K,V>(config).Build();
            if(partitionIds != null && !partitionIds.Any()){
                this.consumer.Assign(GetTopicPartitions(this.topicList, this.partitionIds));
            }
            else{
                this.consumer.Subscribe(topicList);
            }
            return this;
        }
        private List<TopicPartition> GetTopicPartitions(List<string> topics, List<int> partitionIds){
            List<TopicPartition> topicPartitionList = new List<TopicPartition>();
            foreach(string topic in this.topicList){
                foreach(int partitionId in this.partitionIds){
                    TopicPartition partition = new TopicPartition(topic,partitionId);
                    topicPartitionList.Add(partition);
                }
            }
            return topicPartitionList;
        }

        public V Consume(CancellationToken cts)
        {
            //check for config.autocommitenable and call commit
            ConsumeResult<K,V> result = this.consumer.Consume(cts);
            this.latestRecord = result;
            if(this.config.EnableAutoCommit.GetValueOrDefault(false) == false && result.Offset % manualCommitInterval == 0){
                this.Commit();
            }
            Console.WriteLine("Received record from Kafka!");
            Console.WriteLine(result.Value);
            return result.Value;
        }

        public void Close()
        {
            if(this.config.EnableAutoCommit.GetValueOrDefault(false) == false && this.latestRecord != null){
                this.Commit();
            }
            this.consumer.Close();
            this.consumer.Dispose();
        }

        private void Commit(){
            this.consumer.Commit(this.latestRecord);
        }

        public ItemReader<V> DeepClone()
        {
            return this;
            //https://github.com/confluentinc/confluent-kafka-dotnet/issues/812
            //https://github.com/confluentinc/confluent-kafka-dotnet/issues/197
            //https://github.com/confluentinc/confluent-kafka-dotnet/issues/103
            //https://github.com/confluentinc/confluent-kafka-dotnet/issues/654
            //the high level consumer registers in the same group a number of threads equal to 1+N (N equals to the number of brokers)
            //librdkafka internally registers n+1 brokers...hence only one instance of consumer required
            //consumer will be single threaded but with option provided..recommended to use 1 thread
            //processor will work on threadpools
            //return new KafkaItemReader<K,V>(this.topicList, this.config, this.manualCommitInterval, this.partitionIds);
        }
    }
}