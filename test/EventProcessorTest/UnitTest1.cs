using System.Runtime.CompilerServices;
//using System.Xml.Xsl;
using System.Threading.Tasks.Dataflow;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using System.Collections;
using System.Security.Cryptography;
using System.Threading;
using System;
using Xunit;
using Moq;
using Fathom.Precocity.EventProcessor.Component;
using Fathom.Precocity.EventProcessor.Implementation;
using Fathom.Precocity.EventProcessor;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Extensions.Logging;

namespace EventProcessorTest
{
    public class UnitTest1
    {
        [Fact]
        public async void PositiveTest1()
        {
            Object myLock = new Object();
            Random rng = new Random();
            Mock<ItemReader<long>> reader = new Mock<ItemReader<long>>();
            Mock<ItemProcessor<long>> processor = new Mock<ItemProcessor<long>>();
            Mock<IErrorHandler<long>> errorHandler = new Mock<IErrorHandler<long>>();
            ConcurrentBag<long> repository = new ConcurrentBag<long>();
            ConcurrentBag<long> consumerRecords = new ConcurrentBag<long>();
            CancellationTokenSource cts = new CancellationTokenSource();

            long counter = 0;
            reader.Setup(x => x.Consume(It.IsAny<CancellationToken>()))
                .Returns((CancellationToken token) => { lock (myLock) { consumerRecords.Add(++counter); return counter; } });

            reader.Setup(x => x.Close()).Callback(() => Console.WriteLine("Consumer close called"));
            reader.Setup(x => x.DeepClone()).Returns(reader.Object);

            //Simulate load by sleeping for 10 secs
            processor.Setup(y => y.Write(It.IsAny<List<ItemRecord<long>>>()))
                .Callback((List<ItemRecord<long>> recordsProcessor) => { Thread.Sleep(10000); repository.Add(recordsProcessor[0].record); }).Verifiable();
            errorHandler.Setup(z => z.Handle(It.IsAny<Exception>(), It.IsAny<List<ItemRecord<long>>>()))
                .Callback((Exception e, List<ItemRecord<long>> record) => {Console.WriteLine(e.StackTrace);});

            var logger = new LoggerConfiguration().WriteTo.File("/tmp/eventprocessor-test.log").CreateLogger();
                Microsoft.Extensions.Logging.ILogger ilogger = new SerilogLoggerProvider(logger).CreateLogger("EventProcessor");

            Processor<long> eventProcessor = new Processor<long>(reader.Object, processor.Object, errorHandler.Object, ilogger);
            eventProcessor.SetThreadpoolSize(10);
            //await eventProcessor.Start();//use await to wait till kill signal.This is done for testing purpose
            eventProcessor.Start();//Only for testing
            Thread.Sleep(5000);
            eventProcessor.Stop();
            Console.WriteLine(repository.Count);
            //repository.ToList().ForEach((long tmp) => Console.WriteLine(tmp));
            //consumerRecords.ToList().ForEach((long tmp) => Console.WriteLine(tmp));
            Assert.Equal(repository.Count, consumerRecords.Count);//All events have been processed
            Assert.Equal(repository.ToHashSet().SetEquals(consumerRecords.ToHashSet()), true);//Proper data has been captured
            Console.WriteLine(consumerRecords.Count);
        }

        [Fact]
        public async void ExceptionTest1()
        {
            Object myLock = new Object();
            Random rng = new Random();
            Mock<ItemReader<long>> reader = new Mock<ItemReader<long>>();
            Mock<ItemProcessor<long>> processor = new Mock<ItemProcessor<long>>();
            Mock<IErrorHandler<long>> errorHandler = new Mock<IErrorHandler<long>>();
            ConcurrentBag<long> repository = new ConcurrentBag<long>();
            ConcurrentBag<long> consumerRecords = new ConcurrentBag<long>();
            CancellationTokenSource cts = new CancellationTokenSource();

            long counter = 0;
            reader.Setup(x => x.Consume(It.IsAny<CancellationToken>()))
                .Returns((CancellationToken token) => { lock (myLock) { consumerRecords.Add(++counter); return counter; } });

            reader.Setup(x => x.Close()).Callback(() => Console.WriteLine("Consumer close called"));
            reader.Setup(x => x.DeepClone()).Returns(reader.Object);

            //Simulate load by sleeping for 10 secs
            processor.Setup(y => y.Write(It.IsAny<List<ItemRecord<long>>>()))
                .Callback((List<ItemRecord<long>> records) => throw new Exception("Message not processed!")).Verifiable();
            errorHandler.Setup(z => z.Handle(It.IsAny<Exception>(), It.IsAny<List<ItemRecord<long>>>()))
                .Callback((Exception e, List<ItemRecord<long>> record) =>
                            { Console.WriteLine(e.StackTrace + "Record: " + record[0].record); repository.Add(record[0].record); });
                
                var logger = new LoggerConfiguration().WriteTo.File("/tmp/eventprocessor-test.log").CreateLogger();
                Microsoft.Extensions.Logging.ILogger ilogger = new SerilogLoggerProvider(logger).CreateLogger("EventProcessor");
            Processor<long> eventProcessor = new Processor<long>(reader.Object, processor.Object, errorHandler.Object, ilogger);
            eventProcessor.SetThreadpoolSize(10);
            eventProcessor.SetRetries(5);
            //await eventProcessor.Start();//use await to wait till kill signal.This is done for testing purpose
            eventProcessor.Start();//Only for testing
            Thread.Sleep(5000);
            eventProcessor.Stop();
            //Console.WriteLine(repository.Count);
            //repository.ToList().ForEach((long tmp) => Console.WriteLine(tmp));
            //Console.WriteLine("-----");
            Assert.Equal(repository.Count, consumerRecords.Count);//All events have been processed
            Assert.Equal(repository.ToHashSet(), consumerRecords.ToHashSet());//Proper data has been captured
            //Console.WriteLine(consumerRecords.Count);
            //consumerRecords.ToList().ForEach((long tmp) => Console.WriteLine(tmp));
        }
    }
}
